/*
 * Copyright (c) 2017 Tyler Manifold
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>


#define NUM_LANGS 2
#define ARG_START 1

const char *VALID_LANGS[] = { "cpp", "java" };

int tab_spaces = 8;

enum langs
{
	CPP,
	JAVA
};

void usage() {

	printf("skel\n\n");
	
	printf("usage:\n");
	printf("  skel <options> class1 [class2 ...]\n\n");
	
	printf("Options:\n");
	printf("  -c			Specify C++ as the language\n");
	printf("  -j			Specify Java as the language\n");
//	printf("  -s [n]		Use n-number of spaces instead of tabs.\n");
//	printf("			  Default: 8\n");
	printf("\n");
}

void getmacro(char *up) {

	for (int i = 0; i < strlen(up); i++) {

		if (up[i] == '.') {
		
			up[i] = '_';
		
		} else {
			up[i] = toupper(up[i]);
		}
	}
}

/**
 * Print n number of tabs on current line to stdout
 */
void tab(int use_spaces, int n)
{
	switch (use_spaces)
	{
		case 0:
			for (int i = 0; i < n; i++)
			{
				printf("\t");
			}
			break;
		case 1:
			for (int i = 0; i < n * tab_spaces; i++)
			{
				printf(" ");
			}
			break;
		default:
			break;
	}
}

void cpp(char *classnames) {

	int EXT_LEN_CPP = strlen(".cpp");
	int EXT_LEN_H	= strlen(".h");

	char* tok = strtok(classnames, " ");

	while (tok != NULL) {
		
		// generate *.h
		
		int name_len = strlen(tok);

		char header_name[name_len + EXT_LEN_H + 1];

		strcpy(header_name, tok);
		strcat(header_name, ".h");

		header_name[strlen(header_name)] = '\0';
	
	//	printf("%s\n", header_name);
		
		char macro_name[strlen(header_name) + 1];

		strcpy(macro_name, header_name);

		getmacro(macro_name);

		FILE *dot_h = fopen(header_name, "w");

		if (dot_h == NULL) {

			fprintf(stderr, "Unable to open %s\n", header_name);
			exit(1);
		}

		fprintf(dot_h, "\n// %s\n\n", header_name);
		
		fprintf(dot_h, "#ifndef _%s_\n", macro_name);
		fprintf(dot_h, "#define _%s_\n\n", macro_name);
		
		fprintf(dot_h, "class %s \n{\n", tok);

		fprintf(dot_h, "\tpublic:\n");

		fprintf(dot_h, "\t\t %s ();\n", tok);
		fprintf(dot_h, "};\n\n");

		fprintf(dot_h, "#endif // _%s_\n", macro_name);	

		fclose(dot_h);

		// generate *.cpp

		char cpp_name[name_len + EXT_LEN_CPP + 1];

		strcpy(cpp_name, tok);
		strcat(cpp_name, ".cpp");

		cpp_name[strlen(cpp_name)] = '\0';

		FILE *dot_cpp = fopen(cpp_name, "wb");

		if (dot_cpp == NULL) {

			fprintf(stderr, "Unable to open %s\n", cpp_name);
			exit(1);
		}

		fprintf(dot_cpp, "\n// %s\n\n", cpp_name);
		
		fprintf(dot_cpp, "#include \"%s\"\n\n", header_name);

		fprintf(dot_cpp, "%s::%s () \n{\n\n}\n", tok, tok);

		fclose(dot_cpp);

		printf("  %s\n", header_name);
		printf("  %s\n", cpp_name);

		tok = strtok(NULL, " ");
	}


}

void java(char *classnames) {

	// generate *.java
	
	int EXT_LEN = strlen(".java");
	
	char* tok = strtok(classnames, " ");

	while (tok != NULL) {
		int name_len = strlen( tok );
		char java_name[name_len + 1];
		char file_name[name_len + EXT_LEN + 1];

	//	printf ("Copying %s (classnames[%d]) to java_name\n", tok, i);

		strcpy(java_name, tok);
		strcpy(file_name, tok);
		strcat(file_name, ".java");
		
		file_name[strlen(file_name)] = '\0';

		java_name[strlen(java_name)] = '\0';

		//printf("java_name: \'%s\'\n", java_name);

		FILE *dot_java = fopen(file_name, "w");

		if (dot_java == NULL) {

			fprintf(stderr, "Unable to open %s\n", java_name);
			exit(1);
		}

		fprintf(dot_java, "\n// %s\n\n", java_name);
		
		fprintf(dot_java, "public class %s {\n\n", java_name);

		fprintf(dot_java, "\tpublic %s () {\n\n\t}\n}\n", java_name);

		fclose(dot_java);

		printf("  \'%s\'\n", file_name);

		tok = strtok(NULL, " ");
	}
}

int validate_int(char* next_arg)
{
	int valid = 1;
	
	if (next_arg != NULL)
	{
		for (int n = 0; n < strlen(next_arg); n++)
		{
			// the character at next_arg[i] is outside the acceptable range (0 - 9)
			if (next_arg[n] < 0x30 || next_arg[n] > 0x39)
			{
				valid = 0;
			}
		}
	}

	return valid;
}

int main( int argc, char **argv) {


	// There should be at least three arguments
	// skel, --lang, and at least one file
	if (argc < 3) {

		usage();
	
	} else {

		int OUTPUT_LANG = -1;
		int space_tabsize = 8;
		int use_spaces = 0;
		
		char* buf = NULL;

		// begin processing arguments
		for (int i = ARG_START; i < argc; i++)
		{
			char* arg = argv[i];
			// if the character '-' is found, the argument should be a command line option
			if (arg[0] == '-')
			{
				if (strcmp(arg, "-c") == 0)
				{
					OUTPUT_LANG = CPP;
				}
				else if (strcmp(arg, "-j") == 0)
				{
					OUTPUT_LANG = JAVA;
				}
				else if (strcmp(arg, "-s") == 0)
				{
					use_spaces = 1;

					char* next_arg = argv[i+1];

					if (validate_int(next_arg) == 1)
					{
						use_spaces = 1;
						tab_spaces = atoi(next_arg);
						i++;

						//printf(" tab_space: %d\n", tab_spaces);
					}
					else
					{
						printf ("  Invalid number of spaces\n");
						usage();

						exit(1);
					}
				}
				else
				{
					usage();
				}
			}
			else // argument is not an option, treat it as a normal argument
			{				

				if (buf == NULL)
				{
					buf = (char*) malloc(sizeof(char) * strlen(argv[i]) + 1);
					strcpy(buf, argv[i]);
				}
				else
				{
					buf = realloc(buf, sizeof(char) * (strlen(argv[i]) + strlen(buf)) + 2);
					
					strcat(buf, " ");
					strcat(buf, argv[i]);
				}
			}
		}
		

		if ( OUTPUT_LANG == -1 )
		{
			fprintf(stderr, "Error: Language not specified.\n\n");
			usage();
		}
		else
		{
			switch (OUTPUT_LANG)
			{
				case 0:
					cpp(buf);
					break;
				case 1:
					java(buf);
				default:
					break;
			}	
		}

		free(buf);
	}

	return 0;
}
