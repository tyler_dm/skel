
# skel.c makefile

skel :
	gcc skel.c -o skel

clean :
	rm skel *.h *.cpp *.java

remake :
	make clean && make

install :
	sudo cp -v skel /usr/bin/skel

test :
	./skel -c testfile test2 testing3 qwertyuiopasdfghjklzxcvbnm
