# README #

skel is a simple utility used to generate skeleton code for class files in C++ and Java.

	skel
	
	usage
      skel <options> class1 [class2 ...]
	
	Options:
	  -c			Specify C++ as the language
      -j			Specify Java as the language

***

For C++, skel will generate a .h and .cpp file for each classname provided, with the following format:

~~~~
// classname.h

#ifndef _CLASSNAME_H_
#define _CLASSNAME_H_

class classname
{
        public:
                classname ();
};

#endif // _CLASSNAME_H_
~~~~
~~~~
// classname.cpp

#include "classname.h"

classname::classname ()
{

}
~~~~

For Java, skel will produce .java files for each classname with the following format:

~~~~
// classname.java

public class classname {

        public classname () {

        }
}
~~~~
